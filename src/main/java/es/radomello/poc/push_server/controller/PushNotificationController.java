package es.radomello.poc.push_server.controller;

import es.radomello.poc.push_server.model.Subscription;
import es.radomello.poc.push_server.service.PushNotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = {"http://localhost:1234"})
public class PushNotificationController {

	private final PushNotificationService service;

	@PostMapping(path = "/subscriptions", consumes = MediaType.APPLICATION_JSON_VALUE)
	public Mono<ResponseEntity<Void>> subscribe(@RequestBody final Mono<Subscription> subscription) {
		return this.service.subscribe(subscription)
				.map(ResponseEntity::ok);
	}

	@GetMapping(path = "/subscriptions")
	public Mono<ResponseEntity<List<Subscription>>> getSubscription() {
		return this.service.getSubscriptions()
				.collect(Collectors.toList())
				.map(ResponseEntity::ok);
	}

	@GetMapping(path = "/public-key")
	public Mono<ResponseEntity<String>> getPublicKey() {
		return this.service.getPublicKey()
				.map(ResponseEntity::ok);
	}
}

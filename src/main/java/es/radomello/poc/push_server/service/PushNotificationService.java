package es.radomello.poc.push_server.service;

import es.radomello.poc.push_server.model.Subscription;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PushNotificationService {

	@Value("${vapid.private-key}")
	private String privateKey;

	@Value("${vapid.public-key}")
	private String publicKey;

	private final Map<UUID, Subscription> subscriptions = new HashMap<>();

	public Mono<String> getPublicKey() {
		return Mono.just(this.publicKey);
	}

	public Mono<Void> subscribe(final Mono<Subscription> subscription) {
		return subscription
				.doOnNext(s -> this.subscriptions.put(s.id(), s))
				.then();
	}

	public Flux<Subscription> getSubscriptions() {
		return Flux.fromStream(this.subscriptions.keySet().stream()
				.map(this.subscriptions::get));
	}

	public Mono<Void> send(final Mono<UUID> subscriptionId) {

		return subscriptionId
				.map(this.subscriptions::get)
				.doOnNext(this::doSend)
				.then();
	}

	private Mono<Void> doSend(Subscription subscription) {
		final WebClient webClient = WebClient.builder()
				.baseUrl(subscription.url())
				.build();

		final JWTAuth provider = JWTAuth.create(Vertx.vertx(), new JWTAuthOptions()
			.addPubSecKey(new PubSecKeyOptions()
					.setAlgorithm("RS256")
					.setBuffer(this.privateKey))
			.addPubSecKey(new PubSecKeyOptions()
					.setAlgorithm("RS256")
					.setBuffer(this.publicKey)));

		return webClient.post()
				.body(Mono.just(new MessageBody("Prueba", "Prueba de envío de push")), MessageBody.class)
				.header("Authorization", "vapid t=" + provider.generateToken(JsonObject.of()) + ", k=" +
						Base64.getUrlEncoder().withoutPadding().encodeToString(this.publicKey.getBytes(StandardCharsets.UTF_8)))
				.retrieve()
				.bodyToMono(Void.class);
	}

	@Getter
	@RequiredArgsConstructor
	private static class MessageBody {
		private final String title;
		private final String body;
	}
}

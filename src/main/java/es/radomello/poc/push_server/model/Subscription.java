package es.radomello.poc.push_server.model;

import java.util.UUID;

public record Subscription(UUID id, String url, Key key) {

	public record Key(String p256dh, String auth) {
	}
}
